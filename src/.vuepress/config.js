
    const { description } = require("../../package");

    module.exports = {
      /**
       * Ref：https://v1.vuepress.vuejs.org/config/#title
       */
      title: "FlOpEDT Documentation",
      /**
       * Ref：https://v1.vuepress.vuejs.org/config/#description
       */
      description: description,
    
      /**
       * Extra tags to be injected to the page HTML '<head>'
       *
       * ref：https://v1.vuepress.vuejs.org/config/#head
       */
      head: [
        ["meta", { name: "theme-color", content: "#3eaf7c" }],
        ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
        [
          "meta",
          { name: "apple-mobile-web-app-status-bar-style", content: "black" },
        ],
      ],
    
      /**
       * Theme configuration, here is the default theme configuration for VuePress.
       *
       * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
       */
      themeConfig: {
        repo: "",
        editLinks: false,
        docsDir: "",
        editLinkText: "",
        nav: [
          {
            text: "Documentation",
            link: "/documentation/bien-debuter/installation",
          },
          {
            text: "Framagit",
            link: "https://framagit.org/flopedt/FlOpEDT",
          },
          {
            text: "FlOpEDT.org",
            link: "https://www.flopedt.org/",
          },
        ],
        sidebar: {"/documentation/":[{"title":"Bien débuter","collapsable":true,"children":[["/documentation/bien-debuter/installation","Installation"],["/documentation/bien-debuter/import","Import des données dans l'outil"],["/documentation/bien-debuter/deploiement","Déploiement sur un serveur"],["/documentation/bien-debuter/utilisation","Utilisation"]]}]},
      },
    
      /**
       * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
       */
      plugins: ["@vuepress/plugin-back-to-top", "@vuepress/plugin-medium-zoom"],
    };    
    